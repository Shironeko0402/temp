/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

/**
 * change mentee and mentor login
 * 
 * @returns {undefined}
 */
document.getElementById('mentee').onclick = function () {
    // Add border to 'mentee'
    document.getElementById('mentee').style.borderBottom = '2px solid #21a391';
    
    // Remove border from 'mentor'
    document.getElementById('mentor').style.borderBottom = '';
    
    //change roll hidden
    document.getElementById('check-role').value = 'mentee';
};

document.getElementById('mentor').onclick = function () {
    // Add border to 'mentee'
    document.getElementById('mentor').style.borderBottom = '2px solid #21a391';
    
    // Remove border from 'mentor'
    document.getElementById('mentee').style.borderBottom = '';
    
     //change roll hidden
    document.getElementById('check-role').value = 'mentor'; 
};

