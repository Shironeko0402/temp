/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */



const firebaseConfig = {
    apiKey: "AIzaSyBFK-yhIsnRa5d_LVcfWjWSJFTg6MFQohw",
    authDomain: "long-823d0.firebaseapp.com",
    databaseURL: "https://long-823d0-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "long-823d0",
    storageBucket: "long-823d0.appspot.com",
    messagingSenderId: "404674280576",
    appId: "1:404674280576:web:39ce4e1671d7e10d74fa65",
    measurementId: "G-YHZSXJ47J5"
};


// Google sign-in function
firebase.initializeApp(firebaseConfig);

// Google sign-in function
function signInWithGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider)
            .then((result) => {
                // Handle sign-in result
                const user = result.user;
                console.log("Signed in user:", user.email);
                sendEmailToServer(user.email, 'google-login');
                // TODO: Send token to server and verify it
            })
            .catch((error) => {
                console.error("Error during sign-in:", error);
            });
}
// Phone sign-in function
function signInWithPhone() {
    const phoneNumber = "0965186137"; // Replace with the user's phone number including the country code
    const appVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container'); // You can use reCAPTCHA for verification

    firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
            .then((confirmationResult) => {
                // SMS sent successfully
                const code = prompt("Enter the verification code sent to your phone:");
                return confirmationResult.confirm(code);
            })
            .then((result) => {
                // Phone number successfully verified
                const user = result.user;
                console.log("Signed in user:", user.phoneNumber);
                sendPhoneToServer(user.phoneNumber, 'phone-login');
                // TODO: Send token to server and verify it
            })
            .catch((error) => {
                console.error("Error during phone sign-in:", error);
            });
}



function sendEmailToServer(email, action) {
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "login", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            if (this.responseText !== '404') {
                window.location.href = this.responseText;
            } else if (this.responseText === '404') {
                swal("You must register this email first");
            }
        }
    };
    xhr.send("email=" + encodeURIComponent(email) + "&action=" + encodeURIComponent(action));
}

// Add event listener for the sign-in button
document.getElementById("googleSignInButton").addEventListener("click", signInWithGoogle);

