<%-- 
    Document   : form_reset_password
    Created on : Jan 6, 2024, 1:27:34 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="reset_password" method="post">
            <c:choose>
                <c:when test="${param.done != 'true'}">
                    <h1 style="font-size: 25px; font-weight: bold">Reset your password</h1>

                    <p>
                        Enter your email address and we’ll send you a link to reset your password
                    </p>
                    <!-- email input -->
                    <div class="d-flex mt-3 mb-3"
                         style="flex-direction: column">
                        <label 
                            for="email-send-reset"
                            style="font-size: 14px; font-weight: bold; margin-bottom: 5px"
                            >
                            Email
                        </label>
                        <input 
                            class="w-100 py-1 px-2 rounded border border-secondary"
                            id="email-send-reset"
                            type="email" 
                            name="email-send-reset"
                            placeholder="Email"
                            >
                    </div>

                    <button class="w-100 py-2 mt-3 rounded login" 
                            style="color: white"
                            type="submit">
                        Send reset link
                    </button>
                </c:when>

                <c:when test="${param.done == 'true'}">
                    <h3>Check your email</h3>
                    <div style="
                         background-color: #f3faf7;
                         padding: 20px;
                         margin: 10px 0;">
                        <div style="
                             display: flex;
                             gap: 10px;">
                            <svg class="text-success"
                                 viewBox="0 0 20 20"
                                 fill="currentColor"
                                 aria-hidden="true"
                                 style="width: 45px; height: 30px">
                            <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd"></path>
                            </svg>
                            <p style="font-weight: 400; margin: 0">
                                If an account exists for your email,
                                you’ll receive an email with instructions on resetting your password.
                            </p>
                        </div>
                    </div>
                </c:when>
            </c:choose>
                    
            <!-- display error -->        
            <c:if test="${param.done == 'false'}">
                <script>
                    Toastify({
                        text: "Please input email",
                        className: "info",
                        style: {
                            background: "linear-gradient(to right, #00b09b, #96c93d)"
                        }
                    }).showToast();
                </script>
            </c:if>        
            <!-- helper user -->
            <div>
                <div class="my-2">
                    <a href="login"
                       style="color: #21a391; font-size: 14px; font-weight: bold; cursor: pointer;">  
                        Back to log in
                    </a>
                </div>
            </div><!-- end of helper user -->
        </form><!-- end of form -->
    </body>
</html>
