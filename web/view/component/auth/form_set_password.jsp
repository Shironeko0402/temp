<%-- 
    Document   : form_set_password
    Created on : Jan 15, 2024, 12:41:18 AM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="set_password" method="post">
            <h3>Set password</h3>
            <!-- password -->
            <div class="d-flex mt-3 mb-3"
                 style="flex-direction: column">
                <label for="password-reset"
                       style="font-size: 14px; font-weight: bold">
                    Password
                </label>
                <input 
                    class="w-100 py-1 px-2 rounded border border-secondary"
                    id="password-reset"
                    type="password"
                    name="password-reset"
                    >
            </div>

            <!-- confirm password -->
            <div class="d-flex mt-3 mb-3"
                 style="flex-direction: column">
                <label for="password-confirm-reset"
                       style="font-size: 14px; font-weight: bold">
                    Confirm password
                </label>
                <input 
                    class="w-100 py-1 px-2 rounded border border-secondary"
                    id="password-confirm-reset"
                    type="password"
                    name="confirm-password-reset"
                    >
            </div>

            <!-- rules of password -->
            <div class="my-2">
                <ul style="color: graytext">
                    <li>Must be at least 6 characters</li>
                    <li>Must include one uppercase character</li>
                    <li>Must be confirm right password</li>
                </ul>
            </div>

            <div>
                <button class="w-100 py-2 mt-3 rounded login" 
                        style="color: white"
                        type="submit">
                    Save
                </button>
            </div>    
        </form>

        

        <!-- error -->
        <c:if test="${param.done == 'false'}">
            <script>
                Toastify({
                    text: "Please input valid password",
                    className: "info",
                    style: {
                        background: "linear-gradient(to right, #00b09b, #96c93d)"
                    }
                }).showToast();
            </script>
        </c:if>
    </body>
</html>
