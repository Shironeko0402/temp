<%-- 
    Document   : form_register
    Created on : Jan 6, 2024, 12:33:48 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="register" method="post">
            <h1>Mentee Register</h1>
            <input id="check-role" type="text" value="mentee" class="d-none">

            <!-- email input -->
            <div class="d-flex mt-3 mb-3"
                 style="flex-direction: column">
                <label 
                    for="email-register"
                    style="font-size: 14px; font-weight: bold"
                    id="email"
                    >
                    Email
                </label>
                <input 
                    class="w-100 py-1 px-2 rounded border border-secondary"
                    id="email-register"
                    type="email" 
                    name="email-register"
                    >
            </div>

            <!-- password -->
            <div class="d-flex mt-3 mb-3"
                 style="flex-direction: column">
                <label for="password-register"
                       style="font-size: 14px; font-weight: bold">
                    Password
                </label>
                <input 
                    class="w-100 py-1 px-2 rounded border border-secondary"
                    id="password-register"
                    type="password"
                    name="password-register"
                    >
            </div>

            <!-- confirm password -->
            <div class="d-flex mt-3 mb-3"
                 style="flex-direction: column">
                <label for="password-confirm"
                       style="font-size: 14px; font-weight: bold">
                    Confirm password
                </label>
                <input 
                    class="w-100 py-1 px-2 rounded border border-secondary"
                    id="password-confirm"
                    type="password"
                    name="confirm-password"
                    >
            </div>



            <button class="w-100 py-2 mt-3 rounded login" 
                    style="color: white"
                    type="submit">
                Register
            </button>
            
            <!-- rules of password -->
            <div class="my-2">
                <ul style="color: graytext">
                    <li>Must be input valid format email</li>
                    <li>Must be at least 6 characters</li>
                    <li>Must include one uppercase character</li>
                    <li>Must be confirm right password</li>
                </ul>
            </div>
            
            <!-- helper user -->
            <div>
                <div>
                    <div class="my-2">
                        Already have an acccount?
                        <a href="login"
                           style="color: #21a391; font-size: 14px; font-weight: bold; cursor: pointer;">  
                            Log in
                        </a>
                    </div>
                    <div>
                        Apply to be a mentor?
                        <a href="#"
                           style="color: #21a391; font-size: 14px; font-weight: bold; cursor: pointer;">
                            Apply now
                        </a>
                    </div>
                </div>
            </div><!-- end of helper user -->
        </form><!-- end of form -->
    </body>
</html>
