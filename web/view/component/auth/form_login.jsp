<%-- 
    Document   : form_login
    Created on : Jan 5, 2024, 1:36:33 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!-- form login input -->
        <form action="login" method="post">
            <h1>Login</h1>
            <input id="check-role" type="text" value="mentee" class="d-none">

            <!-- Role  -->
            <div class="d-flex mt-3">
                <div id="mentee"
                     class="w-50 text-center"
                     style="padding: 10px 20px; border-bottom: 2px solid #21a391; cursor: pointer">
                    <span>
                        I'm a mentee
                    </span>
                </div>

                <div id="mentor"
                     class="w-50 text-center"
                     style="padding: 10px 20px; cursor: pointer">
                    <span>
                        I'm a mentor
                    </span>
                </div>
            </div><!-- end of chang role div -->

            <!-- email input -->
            <div class="d-flex mt-3 mb-3"
                 style="flex-direction: column">
                <label 
                    for="email"
                    style="font-size: 14px; font-weight: bold"
                    id="email"
                    >
                    Email
                </label>
                <input 
                    class="w-100 py-1 px-2 rounded border border-secondary"
                    id="email"
                    type="email" 
                    name="email"
                    >
            </div>

            <!-- password input -->
            <div class="d-flex mt-3 mb-3"
                 style="flex-direction: column">
                <label for="password"
                       style="font-size: 14px; font-weight: bold">
                    Password
                </label>
                <input 
                    class="w-100 py-1 px-2 rounded border border-secondary"
                    id="password"
                    type="password"
                    name="password"
                    >
            </div>
            <button class="w-100 py-2 mt-3 rounded login" 
                    style="color: white"
                    type="submit">
                Log in
            </button>
            <div class="d-flex align-items-center justify-content-center my-2">
                <p class="longin__style__line">
                    Or
                </p>
            </div>
            <button id="googleSignInButton"
                    class="w-100 py-2 rounded login-gg"
                    style="color: black"
                    type="button">
                <img style="width: 16px; margin-right: 5px"
                     src="https://th.bing.com/th/id/OIP.HgH-NjiOdFOrkmwjsZCCfAHaHl?rs=1&pid=ImgDetMain">
                Log in with Google
            </button>

            <!-- reCAPTCHA container element -->
<!--            <div id="recaptcha-container"></div>-->

            <!-- helper user -->
            <div>
                <div style="margin: 10px 0">
                    <a href="reset_password"
                       style="color: #21a391; font-size: 14px; font-weight: bold; cursor: pointer;">
                        Forgot password?
                    </a>
                </div>
                <div>
                    Don't have an account?
                    <br/>
                    <a href="register"
                       style="color: #21a391; font-size: 14px; font-weight: bold; cursor: pointer;">  
                        Sign up as a mentee
                    </a>
                    <span class="mx-1">or</span>
                    <a href="mentor_register"
                       style="color: #21a391; font-size: 14px; font-weight: bold; cursor: pointer;">
                        apply to be a mentor
                    </a>
                </div>
            </div><!-- end of helper user -->
        </form><!-- end of form -->
    </body>
</html>
