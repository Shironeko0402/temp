<%-- 
    Document   : mentee_profile
    Created on : Jan 8, 2024, 4:16:31 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <!-- Bootstrap v5.3 -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
              rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
              crossorigin="anonymous">

        <!-- css -->
        <style>
            .homepage__browse {
                border: 1px solid white;
                color: #057a6d;
                font-size: 18px;
                font-weight: 500;
                background-color: #def7ec;
                transition-duration: 0.5s;
            }
            .homepage__browse:hover {
                background-color: #84e1bc;
            }

            .mentee__page__navbar li {
                color: white;
                font-weight: 500;
                cursor: pointer;
            }

            .mentee__page__navbar li:hover {
                background-color: #057a6d!important;
            }

            .mentee__profile__formdata label, p {
                font-weight: 500;
            }

        </style>
    </head>
    <body>
        <!-- navbar -->
        <div class="d-flex justify-content-center align-items-center sticky-top" 
             style="width: 100%; padding: 20px 0; border-bottom: 1px solid black; background-color: #172e59">
            <div class="d-flex justify-content-between align-items-center" style="width: 1000px">

                <div class="mentee__page__navbar d-flex align-items-center" style="gap: 10px">
                    <a href="${pageContext.request.contextPath}/">
                        <img src="https://img.icons8.com/?size=256&id=DEg1RKY5gqD7&format=png"
                             width="50px" height="50px" alt="lgo" style="background-color: white; border-radius: 50%"/>
                    </a>
                    <ul style="list-style: none; gap: 30px; margin: 0" class="d-flex justify-content-between align-items-center">
                        <li onclick="changeColorText(this)"
                            class="menteepage__navcontent"
                            style="padding: 10px; border-radius: 10px">
                            Mentors
                        </li>
                        <li onclick="changeColorText(this)"
                            class="menteepage__navcontent"
                            style="padding: 10px; border-radius: 10px">
                            Inquires
                        </li>
                        <li onclick="changeColorText(this)"
                            class="menteepage__navcontent"
                            style="padding: 10px; border-radius: 10px">
                            Wishlists
                        </li>
                        <li onclick="changeColorText(this)" 
                            class="menteepage__navcontent" 
                            style="padding: 10px; background-color: #057a6d; border-radius: 10px">
                            Setting
                        </li>
                    </ul>
                </div>
                <div>
                    <ul class="d-flex justify-content-between align-items-center" style="list-style: none; width: 470px; margin-bottom: 0;">
                        <div style="width: 30px"></div>
                        <li> 
                            <button class="homepage__browse" style="padding: 10px 20px; border-radius: 10px">
                                Browse all mentors
                            </button>
                        </li>
                        <li>
                            <div class="dropdown text-white">
                                <button class="nav-link dropdown-toggle" id="dropdownMenuButton1" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <span style="font-size: 18px; font-weight: 500">
                                        <img src="https://shorturl.at/bnvwK" width="50px" height="50px" style="object-fit: cover; border-radius: 50%">
                                    </span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1" style="padding: 0">
                                    <div style="padding: 4px 16px; border-bottom: 1px solid graytext">
                                        <p style="margin-bottom: 0">Sign as</p>
                                        <span style="font-weight: bold">tranlong280403</span>
                                    </div>

                                    <div>
                                        <li><a class="dropdown-item" href="#">Help center</a></li>
                                        <li><a class="dropdown-item" href="#">Contact</a></li>
                                        <li><a class="dropdown-item" href="${pageContext.request.contextPath}/login" style="color: red; border-top: 1px solid graytext">
                                                Sign out
                                            </a>
                                        </li>
                                    </div>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- profile -->
        <section style="width: 1000px; margin: auto" class="mentee__profile__formdata">
            <h3 class="my-5">Your Profile</h3>
            <div style="border: 1px solid black; padding: 30px 10px; margin-bottom: 100px">
                <form>
                    <!-- photo change -->
                    <div style="display: flex; flex-direction: column; gap: 20px">
                        <p>Photo</p>
                        <div class="d-flex align-items-center" style="gap: 50px">
                            <img id="previewImg" src="" width="60px" height="60px" style="border-radius: 50%">
                            <input type="file" onchange="getImgUrl(event)"">
                        </div>
                        <!-- input name field -->
                        <div class="d-flex align-items-center" style="gap: 30px">
                            <div>
                                <label>First Name *</label>
                                <br/>
                                <input type="text" style="width: 400px; padding: 10px; border-radius: 10px">
                            </div>
                            <div>
                                <label>Last Name *</label>
                                <br/>
                                <input type="text" style="width: 400px; padding: 10px; border-radius: 10px">
                            </div>
                        </div>

                        <!-- input email field -->
                        <div class="d-flex align-items-center" style="gap: 30px">
                            <div>
                                <label>Email *</label>
                                <br/>
                                <input type="email" style="width: 400px; padding: 10px; border-radius: 10px">
                            </div>
                            <div>
                                <label>Location</label>
                                <br/>
                                <select style="width: 400px; padding: 10px; border-radius: 10px; border: 1px solid black">
                                    <option value="hanoi">Ha Noi</option>
                                    <option value="hanoi">Sai Gon</option>
                                    <option value="hanoi">Da Nang</option>
                                </select>
                            </div>
                        </div>

                        <!-- input job title -->
                        <div>
                            <label>Job title</label>
                            <br/>
                            <input type="text" style="width: 830px; padding: 10px; border-radius: 10px">
                        </div>

                        <!-- input link -->
                        <div class="d-flex align-items-center" style="gap: 30px">
                            <div>
                                <label>LinkedIn *</label>
                                <br/>
                                <input type="text" 
                                       placeholder="https://www.linkedin.com/"
                                       style="width: 400px; padding: 10px; border-radius: 10px">
                            </div>
                            <div>
                                <label>Twitter *</label>
                                <br/>
                                <input type="text"
                                       placeholder="https://twitter.com/"
                                       style="width: 400px; padding: 10px; border-radius: 10px">
                            </div>
                        </div>
                    </div>
                    <div class="mt-4">
                        <button style="padding: 10px 20px; border-radius: 10px; background-color: #21a391; font-weight: 500; color: white">
                            Save change
                        </button>
                    </div>
                </form>
            </div>
        </section>                                        
        

        <!-- ALL JAVASCRIPT LINKS -->

        <!-- Bootstrap script v5.3 -->
        <script 
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
            crossorigin="anonymous">
        </script>

        <script>
            function changeColorText(e) {
                const elements = document.querySelectorAll('.menteepage__navcontent');
                for (let i = 0; i < elements.length; ++i) {
                    elements[i].style.background = "#172e59";
                }
                e.style.background = "#057a6d";
            }

            function getImgUrl(e) {
                const input = e.target;
                const previewImg = document.getElementById('previewImg');
                if (input.files && input.files[0]) {
                    const reader = new FileReader();

                    reader.onload = function (e) {
                        previewImg.src = e.target.result;
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
    </body>
</html>
