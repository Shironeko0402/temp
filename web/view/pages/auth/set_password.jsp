<%-- 
    Document   : set_password
    Created on : Jan 15, 2024, 12:39:38 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include file="../../common/header_link.jsp"%>
        <!-- toastify -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastify-js/1.12.0/toastify.js"
                integrity="sha512-ZHzbWDQKpcZxIT9l5KhcnwQTidZFzwK/c7gpUUsFvGjEsxPusdUCyFxjjpc7e/Wj7vLhfMujNx7COwOmzbn+2w=="
                crossorigin="anonymous"
                referrerpolicy="no-referrer">
        </script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
        
        <style>
            .login {
                background-color: #118577;
                border: 1px solid #118577;
            }

            .login:hover {
                transition-duration: 0.5s;
                background-color: #13695f;
                border: 1px solid graytext;
            }

        </style>
    </head>
    <body>
        <!-- layout -->
        <div>
            <div class="w-100 d-flex min-vh-100">
                <!-- left layout -->
                <%@include file="../../common/auth_layout.jsp" %>
                <!-- right layout -->
                <div class="w-75">
                    <div class="justify-content-center align-items-center h-100 d-flex">
                        <div style="width: 380px">
                            <%@include file="../../component/auth/form_set_password.jsp" %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="../../common/footer_link.jsp" %>
    </body>
</html>
