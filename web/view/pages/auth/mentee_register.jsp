<%-- 
    Document   : login
    Created on : Jan 5, 2024, 1:06:47 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mentee Register</title>
        <%@include file="../../common/header_link.jsp"%>
        <style>
            .longin__style__line {
                position: relative;
                color: graytext;
            }

            .longin__style__line:after {
                content: "";
                background-color: graytext;
                position: absolute;
                height: 1px;
                width: 170px;
                bottom: 10px;
                left: 0px;
                margin-left: 20px;
            }

            .longin__style__line:before {
                content: "";
                background-color: graytext;
                position: absolute;
                height: 1px;
                width: 170px;
                bottom: 10px;
                margin-right: 20px;
                right: 0px
            }

            .login {
                background-color: #118577;
                border: 1px solid #118577;
            }

            .login:hover {
                transition-duration: 0.5s;
                background-color: #13695f;
                border: 1px solid graytext;
            }

            .login-gg {
                background-color: white;
                border: 1px solid graytext;
            }

            .login-gg:hover {
                background-color: #f9fafb;
                transition-duration: 0.5s;
            }
        </style>
        <!-- toastify -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastify-js/1.12.0/toastify.js"
                integrity="sha512-ZHzbWDQKpcZxIT9l5KhcnwQTidZFzwK/c7gpUUsFvGjEsxPusdUCyFxjjpc7e/Wj7vLhfMujNx7COwOmzbn+2w=="
                crossorigin="anonymous"
                referrerpolicy="no-referrer">
        </script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    </head>
    <body>
        <!-- layout -->
        <div>
            <div class="w-100 d-flex min-vh-100">
                <!-- left layout -->
                <%@include file="../../common/auth_layout.jsp" %>
                <!-- right layout -->
                <div class="w-75">
                    <div class="justify-content-center align-items-center h-100 d-flex">
                        <div style="height: 550px; width: 380px">
                            <%@include file="../../component/auth/form_mentee_register.jsp" %>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <c:choose>
            <c:when test="${menteeRegisterStatus != null}">
                <script>
                    Toastify({
                        text: "${menteeRegisterStatus}",
                        duration: 3000,
                        position: 'right',
                        style: {
                            background: "linear-gradient(to right, #00b09b, #96c93d)"
                        }
                    }).showToast();
                </script>
            </c:when>
            <c:when test="${registerMenteeSuccess != null}">
                <script>
                    Toastify({
                        text: "${registerMenteeSuccess}",
                        duration: 3000,
                        position: 'right',
                        style: {
                            background: "linear-gradient(to right, #00b09b, #96c93d)"
                        }
                    }).showToast();
                </script>
            </c:when>    
        </c:choose>             

        <%@include file="../../common/footer_link.jsp" %>
    </body>
</html>
