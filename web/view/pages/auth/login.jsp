<%-- 
    Document   : login
    Created on : Jan 5, 2024, 1:06:47 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login | Register</title>
        <%@include file="../../common/header_link.jsp"%>
        
        <!-- style.css -->
        <style>
            .longin__style__line {
                position: relative;
                color: graytext;
            }

            .longin__style__line:after {
                content: "";
                background-color: graytext;
                position: absolute;
                height: 1px;
                width: 170px;
                bottom: 10px;
                left: 0px;
                margin-left: 20px;
            }

            .longin__style__line:before {
                content: "";
                background-color: graytext;
                position: absolute;
                height: 1px;
                width: 170px;
                bottom: 10px;
                margin-right: 20px;
                right: 0px
            }

            .login {
                background-color: #118577;
                border: 1px solid #118577;
            }

            .login:hover {
                transition-duration: 0.5s;
                background-color: #13695f;
                border: 1px solid graytext;
            }

            .login-gg {
                background-color: white;
                border: 1px solid graytext;
            }

            .login-gg:hover {
                background-color: #f9fafb;
                transition-duration: 0.5s;
            }

        </style>

        <!-- firebase -->
        <script src="https://www.gstatic.com/firebasejs/9.0.2/firebase-app-compat.js"></script>
        <script src="https://www.gstatic.com/firebasejs/9.0.2/firebase-firestore-compat.js"></script>
        <script src="https://www.gstatic.com/firebasejs/9.0.2/firebase-auth-compat.js"></script>
        <script src="https://www.google.com/recaptcha/api.js"></script>

        
        <!-- toastify -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastify-js/1.12.0/toastify.js"
                integrity="sha512-ZHzbWDQKpcZxIT9l5KhcnwQTidZFzwK/c7gpUUsFvGjEsxPusdUCyFxjjpc7e/Wj7vLhfMujNx7COwOmzbn+2w=="
                crossorigin="anonymous"
                referrerpolicy="no-referrer">
        </script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    </head>
    <body>
        <div>
            <div class="w-100 d-flex min-vh-100">
                <!-- left layout -->
                <%@include file="../../common/auth_layout.jsp" %>
                <div class="w-75">
                    <div class="justify-content-center align-items-center h-100 d-flex">
                        <div style="height: 550px; width: 380px">
                            <%@include file="../../component/auth/form_login.jsp" %>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- check success login -->                
        <c:if test="${loginStatus == 'failed'}">
            <script>swal("Login failed");</script>
        </c:if>

        <!-- check form validate -->
        <c:if test="${loginStatus == 'errField'}">
            <script>
                Toastify({
                    text: "Please input all fields",
                    duration: 3000,
                    position: 'right',
                    style: {
                        background: "linear-gradient(to right, #00b09b, #96c93d)"
                    }
                }).showToast();
            </script>
        </c:if>


        <!-- check email validate -->
        <c:if test="${loginStatus == 'errMail'}">
            <script>
                Toastify({
                    text: "Wrong email format",
                    duration: 3000,
                    position: 'right',
                    style: {
                        background: "linear-gradient(to right, #00b09b, #96c93d)"
                    }
                }).showToast();
            </script>
        </c:if>

        <script>
            const firebaseConfig = {
                apiKey: "AIzaSyBFK-yhIsnRa5d_LVcfWjWSJFTg6MFQohw",
                authDomain: "long-823d0.firebaseapp.com",
                databaseURL: "https://long-823d0-default-rtdb.asia-southeast1.firebasedatabase.app",
                projectId: "long-823d0",
                storageBucket: "long-823d0.appspot.com",
                messagingSenderId: "404674280576",
                appId: "1:404674280576:web:39ce4e1671d7e10d74fa65",
                measurementId: "G-YHZSXJ47J5"
            };


            // Google sign-in function
            firebase.initializeApp(firebaseConfig);

            // Google sign-in function
            function signInWithGoogle() {
                const provider = new firebase.auth.GoogleAuthProvider();
                firebase.auth().signInWithPopup(provider)
                        .then((result) => {
                            // Handle sign-in result
                            const user = result.user;
                            console.log("Signed in user:", user.email);
                            sendEmailToServer(user.email, 'google-login');
                            // TODO: Send token to server and verify it
                        })
                        .catch((error) => {
                            console.error("Error during sign-in:", error);
                        });
            }

            // Phone sign-in function
            function signInWithPhone() {
                const phoneNumber = "+84982965395"; // Replace with the user's phone number including the country code
                const appVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container'); // You can use reCAPTCHA for verification

                firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
                        .then((confirmationResult) => {
                            // SMS sent successfully
                            const code = prompt("Enter the verification code sent to your phone:");
                            return confirmationResult.confirm(code);
                        })
                        .then((result) => {
                            // Phone number successfully verified
                            const user = result.user;
                            console.log("Signed in user:", user.phoneNumber);
                            // TODO: Send token to server and verify it
                        })
                        .catch((error) => {
                            console.error("Error during phone sign-in:", error);
                        });
            }

            function sendEmailToServer(email, action) {
                let xhr = new XMLHttpRequest();
                xhr.open("POST", "login", true);
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xhr.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        if (this.responseText !== '404') {
                            window.location.href = this.responseText;
                        } else if (this.responseText === '404') {
                            swal("You must register this email first");
                        }
                    }
                };
                xhr.send("email=" + encodeURIComponent(email) + "&action=" + encodeURIComponent(action));
            }

            // Add event listener for the sign-in button
            document.getElementById("googleSignInButton").onclick = signInWithGoogle;
        </script>
        <!-- change mentor and mentee -->
        <script>
            document.getElementById('mentee').onclick = function () {
                // Add border to 'mentee'
                document.getElementById('mentee').style.borderBottom = '2px solid #21a391';

                // Remove border from 'mentor'
                document.getElementById('mentor').style.borderBottom = '';

                //change roll hidden
                document.getElementById('check-role').value = 'mentee';
            };

            document.getElementById('mentor').onclick = function () {
                // Add border to 'mentee'
                document.getElementById('mentor').style.borderBottom = '2px solid #21a391';

                // Remove border from 'mentor'
                document.getElementById('mentee').style.borderBottom = '';

                //change roll hidden
                document.getElementById('check-role').value = 'mentor';
            };
        </script>
        <%@include file="../../common/footer_link.jsp" %>
    </body>
</html>
