<%-- 
    Document   : profile-cv_mentor
    Created on : Jan 16, 2024, 8:21:49 PM
    Author     : hoang
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Profile-cv of Mentor</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

    </head>
    <body>
        <form action="view_profile_of_mentor" method="post">
            <!-- navbar -->
            <div class="d-flex justify-content-center align-items-center sticky-top" 
                 style="width: 100%; padding: 10px 0; border-bottom: 1px solid black; background-color: #f9fafb">
                <div class="d-flex justify-content-between align-items-center" style="width: 1000px">
                    <a href="#">
                        <img src="https://img.icons8.com/?size=256&id=DEg1RKY5gqD7&format=png"
                             width="60px" height="60px" alt="lgo""/>
                    </a>

                    <div>
                        <ul class="d-flex justify-content-between align-items-center" style="list-style: none; width: 470px; margin-bottom: 0">
                            <li>
                                <div class="dropdown">
                                    <button class="nav-link dropdown-toggle" id="dropdownMenuButton1" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        <span style="font-size: 18px; font-weight: 500">Find a mentor</span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="#">Action</a></li>
                                        <li><a class="dropdown-item" href="#">Another action</a></li>
                                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <button class="homepage__browse" style="padding: 10px 20px; border-radius: 10px">
                                    Browse all mentors
                                </button>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/login" style="font-weight: 500; font-size: 18px; text-decoration: none; color: black">
                                    Login
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="border-dark-subtle border-top" style="background: #172e59">
                <div class="container mt-5">
                    <img src="https://placehold.co/100x100/f8f9fa/f8f9fa" id="image" width="200" height="200" class="img-fluid rounded-circle">
                </div>
            </div>
            <div class="container">
                <div class="row mt-3">
                    <div class="col">
                        <h2>Mark Mullinix</h2>
                        <p>Principal @ Growth Vectors</p>
                        <p>Leadership | Start-Ups | Innovation | Loyalty | Financial Services</p>
                        <p>Singapore</p>
                        <p>5.0 (30 reviewers)</p>
                        <p>Active today</p>
                    </div>
                    <div class="col">
                        <p><b>Skills</b></p>
                        <c:forEach begin="1" end="6">
                            <a href="" class="badge bg-light text-wrap text-decoration-none fs-5 text-black fw-normal mt-2">Startups</a>
                        </c:forEach>

                    </div>
                    <div class="col">
                        <div class="card"">
                            <div class="card-body">
                                <h1 class="card-title fw-bold">390$/month</h1>
                                <h6 class="card-subtitle mb-2 text-body-secondary mt-3">33% Discount - 2 x monthly calls, access to my founder-only slack group and other resources.</h6>
                                <p class="card-text">2 calls per month (45min/call)</p>
                                <p class="card-text">Unlimited Q&A via chat</p>
                                <p class="card-text">Expect responses in 24 hours or less</p>
                                <p class="card-text">Hands-on supportt</p>
                                <div class="d-grid gap-2">
                                    <button class="btn btn-primary" type="button" style="background: #118577">Apply now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <h1>About</h1>
                <p>I’m Harry, a top-rated digital marketing mentor and agency mentor, agency owner and entrepreneur.
                    I have over 20 years experience working in digital marketing and have worked with thousands of clients 
                    world-wide of all sizes. I'm trusted by Techstars and Seedstars and multiple incubators and accelerators 
                    as an official mentor.
                </p>
                <h1>What mentees say</h1>
                <c:forEach begin="1" end="3">
                    <div class="card mt-2">
                        <div class="card-header">
                            <ul style="list-style: none;" class="d-flex">
                                <li >
                                    <img src="https://placehold.co/25x25" id="image" width="100" height="100" class="img-fluid rounded-circle">
                                </li>
                                <li class="mt-3 ms-3">
                                    <h4>Mykhaylo</h4>
                                    <label for="star-5" style="color: #ffc107">★★★★★</label>
                                </li>
                            </ul>

                        </div>
                        <div class="card-body">
                            <p class="card-text">My mentorship with Chris was one of the best things I did this year. 
                                Not only did he give very helpful, practical, and precise advice as I improved my graphic design portfolio 
                                during a career shift, but he was very kind and supportive, helping me return to a healthy mindset at a time 
                                when I was feeling discouraged. I have a tendency to plan way ahead, overthink, and get overwhelmed by my own 
                                goals, but he navigated me through organizing those goals into more manageable, actionable, bite-sized chunks. 
                                I'm very grateful for the time I spent working with him!
                            </p>
                        </div>
                    </div>
                </c:forEach>

            </div>
        </form>
        <footer style="width: 1000px; margin: auto; padding-top: 50px">
            <div class="row align-items-start justify-content-center text-center mb-5">
                <div class="col">
                    <h3><span style="color: #057a6d; font-weight: bold; margin-right: 5px">HAPPY</span>PROGRAMMING</h3>
                    <p style="margin: 10px 0">
                        Your trusted source to find highly-vetted mentors & industry professionals to move your career ahead.
                    </p>
                    <div class="homepage__footer__socials">
                        <ul class="d-flex align-items-center p-0 justify-content-evenly" style="list-style: none; gap: 10px">
                            <li style="color: black">
                                <a href="https://www.facebook.com/longkvui" target="blank">
                                    <i class="fa fa-facebook" style="font-size: 30px;"></i> 
                                </a>
                            </li>
                            <li style="color: black">
                                <a href="https://www.instagram.com/lwng284/" target="blank">
                                    <i class="fa fa-instagram" style="font-size: 30px;"></i> 
                                </a>
                            </li>
                            <li style="color: black">
                                <a href="https://www.linkedin.com/in/duylongtd/" target="blank">
                                    <i class="fa fa-linkedin" style="font-size: 30px;"></i> 
                                </a>
                            </li>
                            <li style="color: black">
                                <a href="https://twitter.com/sama/status/1726345564059832609" target="blank">
                                    <i class="	fa fa-twitter" style="font-size: 30px;"></i> 
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <h3>PLATFORM</h3>
                    <ul class="p-0" style="list-style: none;">
                        <li>Browser mentor</li>
                        <li>Book a Session</li>
                        <li>Become a mentor</li>
                    </ul>
                </div>
                <div class="col">
                    <h3>COMPANY</h3>
                    <ul class="p-0" style="list-style: none;">
                        <li>About</li>
                        <li>Privacy Policy</li>
                        <li>Privacy Policy</li>
                    </ul>
                </div>

                <div class="col">
                    <h3>SUPPORT</h3>
                    <ul class="p-0" style="list-style: none;">
                        <li>FAQ</li>
                        <li>Report</li>
                        <li>Contact</li>
                    </ul>
                </div>
            </div>
            <div class="text-center" style="margin-top: 100px; margin-bottom: 50px">
                <span style="font-size: 18px">© 2024 Happy Programming. All Rights Reserved.</span>
            </div>
        </footer>
    </body>
</html>
