<%-- 
    Document   : mentor_register
    Created on : Jan 7, 2024, 1:46:48 PM
    Author     : hoang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mentor Register</title>
        <%@include file="../../common/header_link.jsp"%>
        <link rel="stylesheet" href="./assest/css/style.css"/>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <!-- toastify -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastify-js/1.12.0/toastify.js"
                integrity="sha512-ZHzbWDQKpcZxIT9l5KhcnwQTidZFzwK/c7gpUUsFvGjEsxPusdUCyFxjjpc7e/Wj7vLhfMujNx7COwOmzbn+2w=="
                crossorigin="anonymous"
                referrerpolicy="no-referrer">
        </script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    </head>
    <body>
        <!-- layout -->
        <div class="w-100">
            <div class="w-100 d-flex">
                <!-- left layout -->
                <%@include file="../../common/auth_layout.jsp" %>
                <!-- right layout -->
                <div class="w-75">
                    <div class="justify-content-center align-items-center h-100 d-flex">
                        <div style="width: 380px">
                            <form action="mentor_register" method="post">
                                <h1>Mentor Register</h1>
                                <input id="check-role" type="text" value="mentor" class="d-none">
                                <div class="col-span-2 mb-6">
                                    <!-- upload photo -->
                                    <div class="d-flex mt-3 mb-3"
                                         style="flex-direction: column">
                                        <label 
                                            for="name-register"
                                            style="font-size: 14px; font-weight: bold"
                                            id="name"
                                            >
                                            Upload Photo*
                                        </label>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6 mx-left">
                                                    <img src="https://placehold.co/100x100/f8f9fa/f8f9fa" id="image" width="100" height="100" class="img-fluid rounded-circle">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="file" class="hidden" id="file-input">
                                                </div>

                                            </div>

                                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                                            <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-beta3/js/bootstrap.bundle.min.js"></script>
                                            <script>
                    $(document).ready(function () {
                        // Xử lý khi người dùng chọn tệp
                        $("#file-input").change(function () {
                            // Lấy tệp được chọn
                            var file = this.files[0];

                            // Kiểm tra xem tệp có phải là hình ảnh không
                            if (!file.type.match("image.*")) {
                                alert("Chỉ được tải lên hình ảnh!");
                                return;
                            }

                            // Xử lý tệp
                            var reader = new FileReader();
                            reader.onload = function () {
                                // Lấy dữ liệu của tệp đã được đọc thành thành mảng nhị phân
                                var data = reader.result;

                                // Hiển thị ảnh
                                $("#image").attr("src", data);
                            };
                            reader.readAsDataURL(file);
                        });
                    });
                                            </script>
                                        </div>
                                    </div>
                                    <!-- name input -->
                                    <div class="d-flex mt-3 mb-3" style="flex-direction: column">
                                        <div class="row">
                                            <div class="col">
                                                <label 
                                                    for="firstname-register"
                                                    style="font-size: 14px; font-weight: bold"
                                                    id="firstname"
                                                    >
                                                    First name*
                                                </label>
                                                <input 
                                                    class="w-100 py-1 px-2 rounded border border-secondary"
                                                    id="firstname-register"
                                                    type="name" 
                                                    name="firstname-register"
                                                    >
                                            </div>
                                            <div class="col">     
                                                <label 
                                                    for="lastname-register"
                                                    style="font-size: 14px; font-weight: bold"
                                                    id="lastname"
                                                    >
                                                    Last Name*
                                                </label>
                                                <input 
                                                    class="w-100 py-1 px-2 rounded border border-secondary"
                                                    id="lastname-register"
                                                    type="name" 
                                                    name="lastname-register"
                                                    >
                                            </div>
                                        </div>

                                    </div>
                                    <!-- gender -->
                                    <div class="d-flex mt-3 mb-3"
                                         style="flex-direction: column">
                                        <label 
                                            for="gender-register"
                                            style="font-size: 14px; font-weight: bold"
                                            id="gender"
                                            >
                                            Gender*
                                        </label>
                                        <select id="gender_register" name="gender_register" class="form-select" aria-label="Default select example">
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                    <!-- email input -->
                                    <div class="d-flex mt-3 mb-3"
                                         style="flex-direction: column">
                                        <label 
                                            for="email-register"
                                            style="font-size: 14px; font-weight: bold"
                                            id="email"
                                            >
                                            Email*
                                        </label>
                                        <input 
                                            class="w-100 py-1 px-2 rounded border border-secondary"
                                            id="email-register"
                                            type="email" 
                                            name="email-register"
                                            >
                                    </div>
                                    <!-- phone input -->
                                    <div class="d-flex mt-3 mb-3"
                                         style="flex-direction: column">
                                        <label 
                                            for="phone-register"
                                            style="font-size: 14px; font-weight: bold"
                                            id="phone"
                                            >
                                            Phone*
                                        </label>
                                        <input 
                                            class="w-100 py-1 px-2 rounded border border-secondary"
                                            id="phone-register"
                                            type="number" 
                                            name="phone-register"
                                            >
                                    </div>
                                    <!-- password -->
                                    <div class="d-flex mt-3 mb-3"
                                         style="flex-direction: column">
                                        <label for="password-register"
                                               style="font-size: 14px; font-weight: bold">
                                            Password*
                                        </label>
                                        <input 
                                            class="w-100 py-1 px-2 rounded border border-secondary"
                                            id="password-register"
                                            type="password"
                                            name="password-register"
                                            >
                                    </div>

                                    <!-- confirm password -->
                                    <div class="d-flex mt-3 mb-3"
                                         style="flex-direction: column">
                                        <label for="password-confirm"
                                               style="font-size: 14px; font-weight: bold">
                                            Confirm password*
                                        </label>
                                        <input 
                                            class="w-100 py-1 px-2 rounded border border-secondary"
                                            id="password-confirm"
                                            type="password"
                                            name="confirm-password"
                                            >
                                    </div>
                                    <!-- job tile -->
                                    <div class="d-flex mt-3 mb-3"
                                         style="flex-direction: column">
                                        <label for="job-tile"
                                               style="font-size: 14px; font-weight: bold">
                                            Job tile*
                                        </label>
                                        <input 
                                            class="w-100 py-1 px-2 rounded border border-secondary"
                                            id="job-tile"
                                            type="text"
                                            name="job-tile"
                                            >
                                    </div>
                                    <!-- Skill -->
                                    <div class="d-flex mt-3 mb-3"
                                         style="flex-direction: column">
                                        <label for="skill"
                                               style="font-size: 14px; font-weight: bold">
                                            Skill*
                                        </label>
                                        <input 
                                            class="w-100 py-1 px-2 rounded border border-secondary"
                                            id="skill"
                                            type="text"
                                            name="skill"
                                            >
                                    </div>
                                    <!-- Location -->
                                    <div class="d-flex mt-3 mb-3"
                                         style="flex-direction: column">
                                        <label for="location"
                                               style="font-size: 14px; font-weight: bold">
                                            Location*
                                        </label>
                                        <input 
                                            class="w-100 py-1 px-2 rounded border border-secondary"
                                            id="location"
                                            type="text"
                                            name="location"
                                            >
                                    </div>
                                    <!-- Dob -->
                                    <div class="d-flex mt-3 mb-3"
                                         style="flex-direction: column">
                                        <label for="dob"
                                               style="font-size: 14px; font-weight: bold">
                                            Date of birth*
                                        </label>
                                        <input class="w-100 py-1 px-2 rounded border border-secondary" 
                                               id="dob" type="date" name="dob" min="1900-01-01" max="2100-12-31" >
                                    </div>
                                    <!-- LinkedIn URL -->
                                    <div class="d-flex mt-3 mb-3"
                                         style="flex-direction: column">
                                        <label for="linkedin-url"
                                               style="font-size: 14px; font-weight: bold">
                                            LinkedIn URL*
                                        </label>
                                        <input 
                                            class="w-100 py-1 px-2 rounded border border-secondary"
                                            id="linkedin-url"
                                            type="text"
                                            name="linkedin-url"
                                            >
                                    </div>
                                    <!-- Twitter URL -->
                                    <div class="d-flex mt-3 mb-3"
                                         style="flex-direction: column">
                                        <label for="twitter-url"
                                               style="font-size: 14px; font-weight: bold">
                                            Twitter URL*
                                        </label>
                                        <input 
                                            class="w-100 py-1 px-2 rounded border border-secondary"
                                            id="twitter-url"
                                            type="text"
                                            name="twitter-url"
                                            >
                                    </div>

                                    <button class="w-100 py-2 mt-3 rounded login bg-black" 
                                            style="color: white"
                                            type="submit">
                                        Register
                                    </button>

                                    <!-- rules of password -->
                                    <div class="my-2">
                                        <ul style="color: graytext">
                                            <li>Must be input valid format email</li>
                                            <li>Password must be at least 6 characters</li>
                                            <li>Password must include one uppercase character</li>
                                            <li>Must be confirm right password</li>
                                            <li>Must be input in * form</li>
                                        </ul>
                                    </div>

                                    <!-- helper user -->
                                    <div>
                                        <div>
                                            <div class="my-2">
                                                Already have an acccount?
                                                <a href="login"
                                                   style="color: #21a391; font-size: 14px; font-weight: bold; cursor: pointer;">  
                                                    Log in
                                                </a>
                                            </div>
                                            <div>
                                                Sign up as a mentee?
                                                <a href="#"
                                                   style="color: #21a391; font-size: 14px; font-weight: bold; cursor: pointer;">
                                                    Sign up now
                                                </a>
                                            </div>
                                        </div>
                                    </div><!-- end of helper user -->

                                </div>
                            </form><!-- end of form --> 
                        </div>

                    </div>
                </div>
            </div>
            <c:choose>
                <c:when test="${mentorRegisterStatus != null}">
                    <script>
                        Toastify({
                            text: "${mentorRegisterStatus}",
                            duration: 3000,
                            position: 'right',
                            style: {
                                background: "linear-gradient(to right, #00b09b, #96c93d)"
                            }
                        }).showToast();
                    </script>
                </c:when>
                <c:when test="${registerMentorSuccess != null}">
                    <script>
                        Toastify({
                            text: "${registerMentorSuccess}",
                            duration: 3000,
                            position: 'right',
                            style: {
                                background: "linear-gradient(to right, #00b09b, #96c93d)"
                            }
                        }).showToast();
                    </script>
                </c:when>    
            </c:choose>             

            <%@include file="../../common/footer_link.jsp" %>
    </body>
</html>
