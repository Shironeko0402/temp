<%-- 
    Document   : home_layout
    Created on : Jan 7, 2024, 11:52:02 AM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home page</title>
        <!-- Bootstrap v5.3 -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
              rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
              crossorigin="anonymous">

        <!-- font awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- style_home_page.css -->
        <style>
            .homepage__browse {
                border: 1px solid white;
                color: #057a6d;
                font-size: 18px;
                font-weight: 500;
                background-color: #def7ec;
                transition-duration: 0.5s;
            }

            .homepage__browse:hover {
                background-color: #84e1bc;
            }

            .homepage__search {
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            }

            .homepage__search__btn {
                border: 1px solid white;
                font-size: 18px;
                color: white;
                font-weight: 500;
                background-color: #1c3d7a;
                transition-duration: 0.5s;
                padding: 10px 20px;
                border-radius: 10px;
            }

            .homepage__search__btn:hover {
                background-color: #172e59;
            }

            .homepage__slider a {
                border: 1px solid graytext;
                box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
            }

            .homepage__desc__btn {
                color: white;
                font-size: 18px;
                font-weight: 500;
                background-color: #118577;
                transition-duration: 0.5s;
                padding: 10px 20px;
                border-radius: 10px;
                border: 1px solid #118577;
            }

            .homepage__desc__btn:hover {
                background-color: #13695f;
                border: 1px solid #13695f;
            }

            .homepage__desc__becomeMt {
                color: white;
            }

            .homepage__desc__becomeMt:hover {
                color: graytext;
                transition-duration: 0.5s;
            }
            footer li {
                margin: 10px 0;
                color: graytext;
                font-size: 16px;
                font-weight: 400;
            }

            .homepage__footer__socials a {
                color: black;
            }
            .homepage__footer__socials a:hover {
                color: red;
                transition-duration: 0.5s;
            }


            footer h3 {
                color: black;
                font-size: 22px;
                font-weight: bold;
            }

            footer ul {
                margin: 0;
            }

            footer h1 {
                font-size: 30px;
                margin: 0;
            }

            footer p {
                color: graytext;
                font-size: 16px;
                font-weight: 400;
            }
        </style>
        <!-- google icons -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <!-- slick slider -->
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    </head>
    <body>
        <!-- navbar -->
        <div class="d-flex justify-content-center align-items-center sticky-top" 
             style="width: 100%; padding: 10px 0; border-bottom: 1px solid black; background-color: #f9fafb">
            <div class="d-flex justify-content-between align-items-center" style="width: 1000px">
                <a href="#">
                    <img src="https://img.icons8.com/?size=256&id=DEg1RKY5gqD7&format=png"
                         width="60px" height="60px" alt="lgo""/>
                </a>
                <div>
                    <ul class="d-flex justify-content-between align-items-center" style="list-style: none; width: 470px; margin-bottom: 0">
                        <li>
                            <div class="dropdown">
                                <button class="nav-link dropdown-toggle" id="dropdownMenuButton1" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <span style="font-size: 18px; font-weight: 500">Find a mentor</span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <button class="homepage__browse" style="padding: 10px 20px; border-radius: 10px">
                                Browse all mentors
                            </button>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/login" style="font-weight: 500; font-size: 18px; text-decoration: none; color: black">
                                Login
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- carousel -->
        <section style="width: 1000px; margin: auto; margin-top: 120px" class="d-flex justify-content-center">
            <div class="text-center">
                <p style="font-size: 20px; font-weight: 500">Learn a new skills, launch project, land your dream career</p>
                <h3 class="my-4" style="font-size: 50px; font-weight: bold">
                    1-on-1 <span style="color: #057a6d" id="typewriter-text"></span>
                    <br/>
                    Mentorship
                </h3>
                <div class="homepage__search" style="padding: 10px; border: 1px solid black; border-radius: 10px; width: 600px; flex: 1">
                    <input 
                        type="search"
                        style="border: 1px solid white; width: 400px; padding: 20px 30px; border-radius: 10px; font-size: 20px" 
                        name="name" 
                        placeholder="Search by company, skills">
                    <button class="homepage__search__btn">Search mentors</button>
                </div>
            </div>
        </section>

        <!-- slider mentors -->
        <section class="my-5 autoplay homepage__slider">
            <c:forEach begin="1" end="7">
                <a style="margin: 0 10px;
                   width: 200px; height: 200px;
                   border-radius: 10px;
                   display: flex; flex-direction: column;
                   align-items: center;
                   justify-content: center; text-decoration: none; color: black"
                   href="#">
                    <img
                        style="width: 60px; height: 60px; object-fit: cover; border-radius: 50%"
                        alt="mentor"
                        src="https://shorturl.at/rtBUZ">
                    <h4 class="my-2">Pamyeuoi</h4>
                    <p>Company1</p>
                </a>
            </c:forEach>
        </section>

        <!-- description -->
        <section style="background-color: #172e59; padding: 40px 0">
            <div style="width: 1000px;
                 margin: auto; display: flex;
                 flex-direction: column; align-items: center;
                 justify-content: center; text-align: center; color: white">
                <h1 class="my-4" style="font-weight: bold">At your fingertips: a dedicated career <br/> coach.</h1>
                <p style="font-size: 18px; font-weight: 500">Want to ace your next job interview? Successfully build your startup? 
                    Itching to learn high-demand skills? Work smart with an online mentor or coach by your side 
                    to offer expert advice and guidance to match your zeal. Become unstoppable using MentorCruise.</p>
                <!-- component -->
                <div class="row mt-5">
                    <!-- cpm 1 -->
                    <div class="text-center col" style="display: flex; flex-direction: column; align-items: center">
                        <h3 style="font-size: 30px; font-weight: bold; color: #84e1bc">Find your mentor</h3>
                        <div style="padding: 20px;
                             width: 100px; height: 100px;
                             background-color: #ff5a5f; border-radius: 50%;
                             margin: 20px 0">
                            <i class="material-icons" style="font-size: 60px">group</i>
                        </div>
                        <p style="font-size: 18px; font-weight: 500">
                            Explore our growing catalogue of experienced mentors until you find the perfect fit.
                        </p>
                    </div>

                    <!-- cpm 2 -->
                    <div class="text-center col" style="display: flex; flex-direction: column; align-items: center">
                        <h3 style="font-size: 30px; font-weight: bold; color: #84e1bc">Apply for Mentorship</h3>
                        <div style="padding: 20px;
                             width: 100px; height: 100px;
                             background-color: #f4b941; border-radius: 50%;
                             margin: 20px 0">
                            <i class="material-icons" style="font-size: 60px">contact_mail</i>
                        </div>
                        <p style="font-size: 18px; font-weight: 500">
                            Fill in an application, and if you’re a match 
                            with your chosen mentor you can start your free 7-day trial.
                        </p>
                    </div>

                    <!-- cpm 3 -->
                    <div class="text-center col" style="display: flex; flex-direction: column; align-items: center">
                        <h3 style="font-size: 30px; font-weight: bold; color: #84e1bc">Level up your skills</h3>
                        <div style="padding: 20px;
                             width: 100px; height: 100px;
                             background-color: #4badef; border-radius: 50%;
                             margin: 20px 0">
                            <i class="material-icons" style="font-size: 60px">school</i>
                        </div>
                        <p style="font-size: 18px; font-weight: 500">
                            From personal chats to hands-on support, each mentor offers different services to
                            help you skyrocket your career.
                        </p>
                    </div>
                    <div class="mt-5">
                        <button class="homepage__desc__btn">
                            Find my mentor 
                        </button>
                        <a style="margin-left: 10px;" class="homepage__desc__becomeMt" href="#">
                            Become a mentor
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <footer style="width: 1000px; margin: auto; padding-top: 50px">
            <div class="row align-items-start justify-content-center text-center mb-5">
                <div class="col">
                    <h3><span style="color: #057a6d; font-weight: bold; margin-right: 5px">HAPPY</span>PROGRAMMING</h3>
                    <p style="margin: 10px 0">
                        Your trusted source to find highly-vetted mentors & industry professionals to move your career ahead.
                    </p>
                    <div class="homepage__footer__socials">
                        <ul class="d-flex align-items-center p-0 justify-content-evenly" style="list-style: none; gap: 10px">
                            <li style="color: black">
                                <a href="https://www.facebook.com/longkvui" target="blank">
                                    <i class="fa fa-facebook" style="font-size: 30px;"></i> 
                                </a>
                            </li>
                            <li style="color: black">
                                <a href="https://www.instagram.com/lwng284/" target="blank">
                                    <i class="fa fa-instagram" style="font-size: 30px;"></i> 
                                </a>
                            </li>
                            <li style="color: black">
                                <a href="https://www.linkedin.com/in/duylongtd/" target="blank">
                                    <i class="fa fa-linkedin" style="font-size: 30px;"></i> 
                                </a>
                            </li>
                            <li style="color: black">
                                <a href="https://twitter.com/sama/status/1726345564059832609" target="blank">
                                    <i class="	fa fa-twitter" style="font-size: 30px;"></i> 
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <h3>PLATFORM</h3>
                    <ul class="p-0" style="list-style: none;">
                        <li>Browser mentor</li>
                        <li>Book a Session</li>
                        <li>Become a mentor</li>
                    </ul>
                </div>
                <div class="col">
                    <h3>COMPANY</h3>
                    <ul class="p-0" style="list-style: none;">
                        <li>About</li>
                        <li>Privacy Policy</li>
                        <li>Privacy Policy</li>
                    </ul>
                </div>

                <div class="col">
                    <h3>SUPPORT</h3>
                    <ul class="p-0" style="list-style: none;">
                        <li>FAQ</li>
                        <li>Report</li>
                        <li>Contact</li>
                    </ul>
                </div>
            </div>
            <div class="text-center" style="margin-top: 100px; margin-bottom: 50px">
                <span style="font-size: 18px">© 2024 Happy Programming. All Rights Reserved.</span>
            </div>
        </footer>


        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdn.rawgit.com/mrvautin/typewrite/master/dist/typewrite.min.js"></script>

        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <!-- Slick Slider -->
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <!-- Bootstrap script v5.3 -->
        <script 
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
            crossorigin="anonymous">
        </script>
        <script>
            $(document).ready(function () {
                $('.autoplay').slick({
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 1500,
                    arrows: false
                });
            });
        </script>
        <script>
            function changeText(label) {
                const element = document.getElementById('typewriter-text');
                element.innerHTML += label; // Append each character to the existing content
            }

            function typeWrite() {
                const arrText = ["React", "Java", "Javascript", "Python", "LoL"];

                function typeNextText(index) {
                    const element = document.getElementById('typewriter-text'); // Define element here

                    if (index < arrText.length) {
                        let currentText = arrText[index];

                        for (let i = 0; i < currentText.length; ++i) {
                            setTimeout(() => {
                                changeText(currentText.charAt(i));
                            }, i * 150); // Adjust the delay if needed
                        }

                        setTimeout(() => {
                            element.innerHTML = ''; // Clear the content for the next text
                            typeNextText(index + 1);
                        }, currentText.length * 150 + 1000); // Adjust the delay if needed
                    } else {
                        setTimeout(() => {
                            typeWrite();
                        }, 2000);
                    }
                }

                typeNextText(0);
            }

            typeWrite();
        </script>
    </body>
</html>
