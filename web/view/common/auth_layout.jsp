<%-- 
    Document   : login_layout
    Created on : Jan 6, 2024, 12:16:42 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="w-25">
            <div class="bg-black h-100 d-flex justify-content-center align-items-center">
                <div 
                    style="width: 100px;
                    height: 100px;
                    background-color: white;
                    border-radius: 50%"
                    class="d-flex
                    align-items-center
                    justify-content-center
                    "
                    >
                    <i class="material-icons" style="font-size: 50px">lock_outline</i>
                </div>
            </div>
        </div>
    </body>
</html>
