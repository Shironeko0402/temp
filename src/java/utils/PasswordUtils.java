/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import org.mindrot.jbcrypt.BCrypt;


/**
 *
 * @author admin
 */
public class PasswordUtils {
    
    public static String hashPassword (String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }
    
    public static boolean verifyPassword(String passwordInputed, String hashedPassword) {
        return BCrypt.checkpw(passwordInputed, hashedPassword);
    }
}
