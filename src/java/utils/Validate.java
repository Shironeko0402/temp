/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import constant.IConst;

/**
 *
 * @author admin
 */
public class Validate {

    /**
     * The function to validate email
     *
     * @param email value of email
     * @return {@code true} if matched, otherwise {@code false}
     */
    public static boolean isMail(String email) {
        return email.matches(IConst.REGEX_EMAIL);
    }

    public static boolean checkValidPassword(String password) {
        return password.length() >= 6 && isContainsUppercase(password);
    }
    
    public static boolean confirmPassword (String password, String confirm) {
        return confirm.equals(password);
    }
   
    private static boolean isContainsUppercase(String password) {
        for (int i = 0; i < password.length(); ++i) {
            if (Character.isUpperCase(password.charAt(i))) {
                return true;
            }
        }
        return false;
    }
}
