/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.UserDao;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import utils.PasswordUtils;
import utils.Validate;

/**
 *
 * @author hoang
 */
public class MentorRegisterController extends HttpServlet {
    
    private UserDao userDao;

    @Override
    public void init() throws ServletException {
        this.userDao = new UserDao();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MentorRegisterController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MentorRegisterController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("view/pages/auth/mentor_register.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            String email = request.getParameter("email-register");
            String password = request.getParameter("password-register");
            String confirmPassword = request.getParameter("confirm-password");
            String firstname = request.getParameter("firstname-register");
            String lastname = request.getParameter("lastname-register");
            String gender = request.getParameter("gender_register");
            String phone = request.getParameter("phone-register");
            String jobTile= request.getParameter("job-tile");
            String skill = request.getParameter("skill");
            String location = request.getParameter("location");
            String linkedIn = request.getParameter("linkedin-url");
            String twitter = request.getParameter("twitter-url");
            String date = request.getParameter("dob");
            
            //validate fields
            if (email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() ||
                    firstname.isEmpty() || lastname.isEmpty() || gender.isEmpty() ||
                    phone.isEmpty() || jobTile.isEmpty() || skill.isEmpty() ||
                    location.isEmpty() || linkedIn.isEmpty() || twitter.isEmpty() ) {
                request.setAttribute("mentorRegisterStatus", "Must be input all fields");
                request.getRequestDispatcher("view/pages/auth/mentor_register.jsp").forward(request, response);
                return;
            } else if (!Validate.isMail(email)) {
                request.setAttribute("mentorRegisterStatus", "Wrong format email");
                request.getRequestDispatcher("view/pages/auth/mentor_register.jsp").forward(request, response);
                return;
            } else if (!Validate.confirmPassword(password, confirmPassword)) {
                request.setAttribute("mentorRegisterStatus", "Wrong confirm password");
                request.getRequestDispatcher("view/pages/auth/mentor_register.jsp").forward(request, response);
                return;
            } else if (!Validate.checkValidPassword(password)) {
                request.setAttribute("mentorRegisterStatus", "Invalid password, see the rules");
                request.getRequestDispatcher("view/pages/auth/mentor_register.jsp").forward(request, response);
                return;
            }
            String passwordHashed = PasswordUtils.hashPassword(password);
            boolean ge;
            ge = gender.equals("Male");
            LocalDate dob = LocalDate.parse(date);
            java.sql.Date dobSql = java.sql.Date.valueOf(dob);
            
            //create user
            if (userDao.createMentor(email, passwordHashed, phone, firstname, lastname, ge, jobTile, linkedIn, twitter, location, dobSql)) {
                request.setAttribute("registerMentorSuccess", "Register successfully");
            } else {
                request.setAttribute("registerMentorSuccess", "Email has existed");
            }
            request.getRequestDispatcher("view/pages/auth/mentor_register.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
