/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.UserDao;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import utils.Validate;

/**
 *
 * @author admin
 */
public class LoginController extends HttpServlet {

    public UserDao userDAO;

    @Override
    public void init() {
        this.userDAO = new UserDao();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //check user login or not
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        //if null we go to file login
        if (user == null) {
            request.getRequestDispatcher("view/pages/auth/login.jsp").forward(request, response);
        } else {
            //if not null we go to file based on role
            switch (user.getRole()) {
                case "mentee":
                    response.sendRedirect("view/pages/home/mentee/menteehome.jsp");
                    break;
                case "mentor":
                    response.sendRedirect("view/pages/home/mentor/mentorhome.jsp");
                    break;
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String action = request.getParameter("action");
        
        //check user sign in google or not
        //action == null -> login with default
        if (action == null) {
            //validate
            if (email.isEmpty() || password.isEmpty()) {
                request.setAttribute("loginStatus", "errField");
                request.getRequestDispatcher("view/pages/auth/login.jsp").forward(request, response);
                return;
            }
            if (!Validate.isMail(email)) {
                request.setAttribute("loginStatus", "errMail");
                request.getRequestDispatcher("view/pages/auth/login.jsp").forward(request, response);
                return;
            } 
        }
        //check user with authenticate
        User user = this.userDAO.authenticate(email, password, action);
        
        //if not null we set session and based on role to sendredirect
        if (user != null) {
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            session.setMaxInactiveInterval(5);
            switch (user.getRole()) {
                case "mentee":
                    if (action != null) {
                        response.getWriter().write("view/pages/home/mentee/menteehome.jsp");
                    } else {
                        response.sendRedirect("view/pages/home/mentee/menteehome.jsp");
                    }
                    break;
                case "mentor":
                    if (action != null) {
                        response.getWriter().write("view/pages/home/mentor/mentorhome.jsp");
                    } else {
                        response.sendRedirect("view/pages/home/mentor/mentorhome.jsp");
                    }
                    break;
            }
        } else { 
            //if null we go back login
            //case 1 login default but password not correct
            if (action == null) {
                request.setAttribute("loginStatus", "failed");
                request.getRequestDispatcher("view/pages/auth/login.jsp").forward(request, response);
            } 
            //case 2 login with google but email not existed in database
            else {
                response.getWriter().write("404");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
