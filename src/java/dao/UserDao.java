/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import dao.daoImpl.IUserDao;
import context.DBContext;
import entity.User;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.PasswordUtils;

/**
 *
 * @author admin
 */
public class UserDao extends DBContext implements IUserDao {

    public String getPasswordByEmail(String email) {
        try {
            String query = "SELECT password FROM users WHERE email = ?";
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setString(1, email);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                return result.getString("password");
            }
            return null;
        } catch (SQLException ex) {
            return null;
        }
    }

    @Override
    public User authenticate(String email, String password, String action) {
        //check hashed password if not matched return null
        if (action == null) {
            String passwordHashed = getPasswordByEmail(email);
            if (!PasswordUtils.verifyPassword(password, passwordHashed)) {
                return null;
            }
        }
        try {
            User user;
            String query;
            PreparedStatement stm;
            query = "SELECT * FROM users WHERE email = ?";
            stm = connection.prepareStatement(query);
            stm.setString(1, email);
            ResultSet result = stm.executeQuery();
            if (result.next()) {
                user = new User(
                        result.getInt(1),
                        result.getString(2),
                        result.getString(3),
                        result.getString(4),
                        result.getString(5),
                        result.getTimestamp(6),
                        result.getTimestamp(7)
                );
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean createMentee(String email, String password) {
        try {
            String query = "INSERT INTO users (email, password, role) VALUES (?, ?, ?)";
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setString(1, email);
            stm.setString(2, password);
            stm.setString(3, "mentee");
            return stm.executeUpdate() > 0;
        } catch (SQLException ex) {
            return false;
        }
    }
    
    
    public boolean createMentor(String email, String password, String phone, String firstname, String lastname, 
            boolean gender, String job_tile, String linkedin_url, String twitter_url, String location, java.sql.Date dob) {
        try {
            String query = "INSERT INTO users (email, password, role, phone, first_name, last_name,"
                    + "gender, job_title, linkedin_url, twitter_url, location, dob) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setString(1, email);
            stm.setString(2, password);
            stm.setString(3, "mentor");
            stm.setString(4, phone);
            stm.setString(5, firstname);
            stm.setString(6, lastname);
            stm.setBoolean(7, gender);
            stm.setString(8, job_tile);
            stm.setString(9, linkedin_url);
            stm.setString(10, twitter_url);
            stm.setString(11, location);
            stm.setDate(12, dob);
            return stm.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    @Override
    public boolean verifyToken (String token) {
        try {
            String query = "SELECT * FROM users WHERE token_change_password = ?";
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setString(1, token);
            ResultSet result = stm.executeQuery();
            while (result.next()) {                
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    @Override
    public void updatePassword (String password, String token) {
        try {
            String query = "UPDATE users SET token_change_password = ?, password = ? where token_change_password = ?";
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setString(1, null);
            stm.setString(2, password);
            stm.setString(3, token);
            stm.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void updateToken(String token, String email) {
        try {
            String query = "UPDATE users SET token_change_password = ? where email = ?";
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setString(1, token);
            stm.setString(2, email);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
