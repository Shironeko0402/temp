/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package dao.daoImpl;

import entity.User;

/**
 *
 * @author admin
 */
public interface IUserDao {
    User authenticate (String email, String password, String action);
    boolean createMentee (String email, String password);
    void updateToken (String token, String email);
    boolean verifyToken (String token);
    void updatePassword (String password, String token);
}
